
library(tidyverse)
pdf("visualization.pdf")

dataset <- read_csv("dataset.csv", col_names = FALSE)
dfTsc<-as.data.frame((dataset))
dfTsc<-dfTsc[-1,]
names(dfTsc)[2] <- "Number_Of_Claims"
names(dfTsc)[3] <- "Number_of_Business_Properties"
dfTsc$Number_Of_Claims<-as.numeric(dfTsc$Number_Of_Claims)
dfTsc$Number_of_Business_Properties<-as.numeric(dfTsc$Number_of_Business_Properties)
cor(dfTsc$Number_Of_Claims,dfTsc$Number_of_Business_Properties,use="pairwise.complete.obs")
#plot(dfTsc$Number_Of_Claims,dfTsc$Number_of_Business_Properties,xlab="Number_Of_Claims Average Per Item Marked",ylab="Number_of_Business_Properties Average Per Item Marked",main="Number_Of_Claims vs Teacher marks")
plot(jitter(dfTsc$Number_of_Business_Properties,1),dfTsc$Number_Of_Claims,xlab="Number of Business Properties",ylab="Number Of Claims", main="Number Of Claims vs Number of Business Properties ")
abline(lm(dfTsc$Number_Of_Claims ~ dfTsc$Number_of_Business_Properties))

#hist(dfTsc$Number_of_Business_Properties)

stdMean<-mean(dfTsc$Number_Of_Claims,na.rm=TRUE)
stdSD<-sd(dfTsc$Number_Of_Claims,na.rm=TRUE)
Number_of_Business_PropertiesMean<-mean(dfTsc$Number_of_Business_Properties,na.rm=TRUE)
Number_of_Business_PropertiesSD<-sd(dfTsc$Number_of_Business_Properties,na.rm=TRUE)

hist(dfTsc$Number_Of_Claims,freq=F, main ="Frequency Distribution", xlab="Number Of Business Properties", ylab="Number of Claims")
lines(seq(1, 140, by=.1), dnorm(seq(1, 140, by=.1), stdMean, stdSD), col="blue")

dev.off()
